using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LiveTextScript : MonoBehaviour
{
    Text text;
    public static int LiveAmount = 3;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
        if (LiveAmount < 1) { SceneManager.LoadScene("DEATH"); }
    }

    // Update is called once per frame
    void Update()
    {
        text.text = LiveAmount.ToString();
    }
}


